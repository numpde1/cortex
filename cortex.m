classdef cortex < handle
% - build system from cortex image data
% - solve for discrete Laplacian
% - compute cortex thickness

    properties
        A_h %: NxN system matrix
        F_h %: Nx1 weight vector
        D %: domain matrix
        U %: matrix solution in domain
        sol %: Nx1 solution vector: A_h*sol = F_h
        path %: path to image data
        
        N %: system size
        D_active %: vector of linear domain indices with posive values
        outer_bndry %: set of outer boundary points of D
        inner_bndry %: set of outer boundary points of D
        dx_U, dy_U %: gradient field of U
    end
    
    methods
        function obj = cortex(i_path)
            obj.path = i_path;
            obj.i_read;
        end
        function i_read(obj)
        % read image data and build domain matrix
            obj.D = double(imread(obj.path));
            obj.D_active = find(obj.D);

            obj.N = length(obj.D_active);
            count_vec = [1:obj.N];
            obj.D(obj.D_active) = count_vec;
        end
        
        function build_system(obj)
        % construct the system matrix A_h and the weight vector F_h
            obj.A_h = delsq(obj.D);
            obj.F_h = zeros(obj.N,1);
            obj.build_bndry;
        end
        function build_bndry(obj)
            bndry_pts = bwboundaries(obj.D);
            obj.build_outer_bndry(bndry_pts);
            obj.build_inner_bndry(bndry_pts);
            obj.set_bndry_data(obj.outer_bndry);
        end
        function build_outer_bndry(obj,bndry_pts)
            obj.outer_bndry = bndry_pts{1}; 
            obj.outer_bndry = sortrows(obj.outer_bndry,[2,1]); 
            obj.outer_bndry = obj.outer_bndry(2:end,:);
        end        
        function build_inner_bndry(obj,bndry_pts)
            obj.inner_bndry = bndry_pts{end}; 
            obj.inner_bndry = sortrows(obj.inner_bndry,[2,1]); 
            obj.inner_bndry = obj.inner_bndry(2:end,:);
        end
        function set_bndry_data(obj, outer_bndry)
            [n,m] = size(obj.D);
            k = 0; idx_to_bndry_pt = 1;
            for j=1:m
                for i=1:n
                    if obj.D(i,j)~=0
                        k = k+1; 
                        if outer_bndry(idx_to_bndry_pt,:) == [i,j]
                            obj.A_h(k,:) = zeros(1,obj.N);
                            obj.A_h(k,k) = 1;
                            obj.F_h(k) = 1;
                            idx_to_bndry_pt = idx_to_bndry_pt+1;
                        end
                    end
                end
            end        
        end
        
        function solve_system(obj)
            obj.sol = obj.A_h\obj.F_h;
            obj.U = obj.D;
            obj.U(obj.D>0) = obj.sol(obj.D(obj.D>0));
        end
        
        function T = cortex_thickness(obj)
            obj.calculate_gradient;
            T = 999;
            for i = 1:length(obj.outer_bndry)
                outer_x = obj.outer_bndry(i,1);
                outer_y = obj.outer_bndry(i,2);
                t = obj.streamline(outer_x, outer_y);
                if t<T
                    T = t;
                end
            end
        end
        
        function calculate_gradient(obj)
            % MATLAB's gradient uses the mid point rule 
            % => wrong at the boundary
            [obj.dx_U, obj.dy_U] = gradient(obj.U);
            obj.dx_U(abs(obj.dx_U)==0.5) = 0;
            obj.dy_U(abs(obj.dy_U)==0.5) = 0;
            for i = 1:length(obj.outer_bndry)
                x_0 = obj.outer_bndry(i,1);
                y_0 = obj.outer_bndry(i,2);
                obj.one_step_gradient_x(x_0,y_0);
                obj.one_step_gradient_y(x_0,y_0);
            end
            %obj.normalize_gradient;
        end
        function one_step_gradient_x(obj,x_0,y_0)
                if obj.U(x_0-1,y_0)>0
                    obj.dx_U(x_0,y_0) = obj.U(x_0,y_0) - obj.U(x_0-1,y_0);
                else
                    obj.dx_U(x_0,y_0) = obj.U(x_0+1,y_0) - obj.U(x_0,y_0);
                end            
        end
        function one_step_gradient_y(obj,x_0,y_0)
                if obj.U(x_0,y_0-1)>0
                    obj.dx_U(x_0,y_0) = obj.U(x_0,y_0) - obj.U(x_0,y_0-1);
                else
                    obj.dx_U(x_0,y_0) = obj.U(x_0,y_0+1) - obj.U(x_0,y_0);
                end
        end
        
        function normalize_gradient(obj)
            [n,m] = size(obj.D);
            for i=1:n
                for j=1:m
                    dx = obj.dx_U(i,j); dy = obj.dy_U(i,j);
                    norm = dx*dx + dy*dy; norm = sqrt(norm);
                    if norm~=0
                        obj.dx_U(i,j) = obj.dx_U(i,j)*1./norm;
                        obj.dy_U(i,j) = obj.dy_U(i,j)*1./norm;
                    end
                end
            end
        end
        
        function distance = streamline(obj, outer_x, outer_y)
            distance = 0; norm = 1;
            x = outer_x; y = outer_y;
            while norm > 0.0001;
                [dx,dy] = obj.one_step_streamline(x,y);
                norm = sqrt(dx*dx + dy*dy); distance = distance + 1;
                x = x-dx*1./norm; y = y-dy*1./norm;
            end
        end
        function [dx, dy] = one_step_streamline(obj, x,y)
            dx = obj.dx_U(round(x),round(y)); 
            dy = obj.dy_U(round(x),round(y));
        end
        function bool = is_near_inner_bndry(obj,x,y)
            
            bool = 0;
        end
    end
end

