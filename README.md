# README #

### What is this repository for? ###

* Our Cortex project for NumPDE1
* Version 0.1

### How do I get set up? ###

* pull the repository

### Contribution guidelines ###

* do not work on master branch. create your own branch. use descriptive names.
* document your changes.
* follow the given program structure laid out in main.m